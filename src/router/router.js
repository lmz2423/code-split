import a from '../componets/a.vue';
//import b from '../componets/b.vue';
// const b = ()=> import('../componets/b.vue');
const b = ()=> import(/*webpackChunkName:'b' */'../componets/b.vue');
//import c from '../componets/c.vue';
// const c = ()=> import('../componets/c.vue');
const c = ()=> import(/*webpackChunkName:'c'*/'../componets/c.vue');
export default[
    {
        path:'/',
        component:a
    },
    {
        path:'/b',
        component:b
    },
    {
        path:'/c',
        component:c
    }
]