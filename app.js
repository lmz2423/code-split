const express = require('express');
const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');

//从webpack.config.js文件中读取WebPack配置

const config = require('./webpack.config.js');

const app = express();
const compiler = webpack(config);

app.use(webpackMiddleware(compiler));

//启动HTTP服务器
app.listen(3000);