import Vue from "vue"
import app from './app.vue'
import VueRouter from 'vue-router'
import routes from './router/router.js'

Vue.use(VueRouter);
const router = new VueRouter({routes});
new Vue({
    router,
    el:'#test',
    template:'<App/>',
    render: h =>h(app)
})