const path  = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    entry:{
        main:'./src/main.js',
        base:[
            'vue',
            'vue-router'
        ]
    },
    module:{
        loaders:[
            {
              test: /\.vue$/,
              loader: 'vue-loader',
              options:{
                extractCSS:false,
                esModule:false
            }
          },{
              test:/\.js$/,
              loader:'babel-loader',
              exclude:/node_modules/
          },{
              test:/\.css$/,
              loader:'css-loader'
          }]
    },
    plugins:[
        new webpack.optimize.UglifyJsPlugin(),
        new HtmlWebpackPlugin({
            template:'./src/index.html',
            filename:'index.html'
        }),
        new webpack.HashedModuleIdsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name:'base',
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name:'manifest',
            chunks:['base']
        })
    ],
    output:{
        path:path.resolve(__dirname,'dist'),
        filename:'[name]-[chunkhash].js',
        chunkFilename:'[name]-[chunkhash].bundle.js',
        publicPath:'/',
        //为前端异常监控，跨域的js脚本不报script error错误 
        crossOriginLoading:"anonymous"
    },
    devServer:{
        contentBase: path.join(__dirname,'dist'),
        compress:true,
    }
}